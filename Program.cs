﻿//------------------------------------------------------------------
// NavisWorks Sample code
//------------------------------------------------------------------

// (C) Copyright 2009 by Autodesk Inc.

// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted,
// provided that the above copyright notice appears in all copies and
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting
// documentation.

// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS.
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//------------------------------------------------------------------
//
// This sample demonstrates how to use the Automation parts of the API
// to open two AutoCad DWG files in Navisworks and save as a single 
// Navisworks file. All without showing the GUI.
//
//------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using Autodesk.Navisworks.Api.Automation;

namespace FileManipulation
{
    class Program
    {

        static void Main(string[] args)
        {
            #region OpenSaveHelloWorld
            NavisworksApplication automationApplication = null;

            //Get Current Working directory
            Assembly exe = Assembly.GetExecutingAssembly();
            if (args.Count() != 0)
            {
                string workingDir = args[0];

                //Retrive all Revit file path
                string nwcListPath = Path.Combine(workingDir, "RevitFilesList.txt");
                Dictionary<string, string> nwcPaths = GetMapping(nwcListPath);

                Console.WriteLine("Opening " + nwcListPath);

                string nwfListPath = Path.Combine(workingDir, "NWFFilesList.txt");
                Dictionary<string, string> nwfPaths = GetMapping(nwfListPath);

                Console.WriteLine("Opening " + nwfListPath);

                try
                {
                    //create NavisworksApplication automation object
                    automationApplication = new NavisworksApplication();

                    Console.WriteLine("Opening Navisworks");

                    //disable progress whilst we do this procedure
                    automationApplication.DisableProgress();

                    //Create NWC files
                    Dictionary<string, string> failedNWCPaths = CreateNWC(automationApplication, nwcPaths);

                    if (failedNWCPaths.Count != 0)
                    {
                        TimeSpan span = new TimeSpan(0, 10, 0);
                        System.Threading.Thread.Sleep(span);
                        CreateNWC(automationApplication, failedNWCPaths);
                    }

                    //Update NWF and export to NWD
                    Dictionary<string, string> failedNWDPaths = CreateNWD(automationApplication, nwfPaths);

                    if (failedNWDPaths.Count != 0)
                    {
                        TimeSpan span = new TimeSpan(0, 10, 0);
                        System.Threading.Thread.Sleep(span);
                        CreateNWD(automationApplication, failedNWDPaths);
                    }

                    //Re-enable progress
                    automationApplication.EnableProgress();
                }
                catch (Autodesk.Navisworks.Api.Automation.AutomationException e)
                {
                    //An error occurred, display it to the user
                    System.Windows.Forms.MessageBox.Show("Error: " + e.Message);
                }
                catch (Autodesk.Navisworks.Api.Automation.AutomationDocumentFileException e)
                {
                    //An error occurred, display it to the user
                    System.Windows.Forms.MessageBox.Show("Error: " + e.Message);
                }
                finally
                {
                    if (automationApplication != null)
                    {
                        automationApplication.Dispose();
                        automationApplication = null;
                    }
                }
            }
            else
            {
                Console.WriteLine("please define working directory in argument");
            }

            #endregion
        }


        private static Dictionary<string, string> CreateNWC(NavisworksApplication automationApplication, Dictionary<string, string> nwcPaths)
        {
            //Create NWC files
            int fileNumber = nwcPaths.Count;
            int i = 1;

            Dictionary<string, string> failedNWCPaths = new Dictionary<string, string>();

            //Loop on all file path
            foreach (string path in nwcPaths.Keys)
            {
                string nwcFilePath = Path.ChangeExtension(path, ".nwc");
                string resultPath = nwcPaths[path];

                try
                {
                    automationApplication.CreateCache(path);

                    if (File.Exists(resultPath))
                    {
                        File.Delete(resultPath);
                    }

                    File.Move(nwcFilePath, resultPath);
                    Console.WriteLine("File " + i + " upon " + fileNumber + " - " + resultPath);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("File " + i + " upon " + fileNumber + " - " + resultPath + " - " + ex.GetType().ToString() + " - " + ex.Message);
                    failedNWCPaths.Add(path, resultPath);
                }

                i++;
            }

            return failedNWCPaths;

        }

        private static Dictionary<string, string> CreateNWD(NavisworksApplication automationApplication, Dictionary<string, string> nwfPaths)
        {
            //Update NWF and export to NWD
            int fileNumber = nwfPaths.Count;
            int i = 1;
            Dictionary<string, string> failedNWDPaths = new Dictionary<string, string>();

            foreach (string path in nwfPaths.Keys)
            {
                
                try
                {
                    automationApplication.OpenFile(path);
                    automationApplication.SaveFile(nwfPaths[path]);
                    Console.WriteLine("File " + i + " upon " + fileNumber + " - " + nwfPaths[path]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("File " + i + " upon " + fileNumber + " - " + nwfPaths[path] + " - " + ex.GetType().ToString() + " - " + ex.Message);
                    failedNWDPaths.Add(path, nwfPaths[path]);
                }

                i++;
            }

            return failedNWDPaths;

        }

        private static Dictionary<string, string> GetMapping(string filePath)
        {
            //Retrive all file path, along with their resulting path

            string[] nwfPaths = File.ReadAllLines(filePath, Encoding.GetEncoding("iso-8859-1"));

            Dictionary<string, string> fileMapping = new Dictionary<string, string>();

            foreach (string line in nwfPaths)
            {
                string[] paths = line.Split(';');
                fileMapping.Add(paths[0], paths[1]);
            }

            return fileMapping;
        }
    }


}
